<?php
	include '../../utils/verificarSessao.php';
    sessionCheck('../../index.php');
    $id = '';
	$nome = '';
	$nascimento = '';
	$sexo = 'Escolha...';
	$anoletivo = 'Escolha...';
	$button = 'Cadastrar';
	$action = "../../routes/alunoCadastrado.php";
	$action_back = "../../menu.php";
	$title = "Cadastrar Aluno";
	if(isset($_GET['id'])) {
		include '../../../Backend/controllers/readAlunos.php';
		foreach (json_decode(buscarAluno()) as $aluno ) {
			$id = $aluno->id;
		    $nome = $aluno->nome;
		    $nascimento = $aluno->nasc;
		    $sexo = $aluno->sexo;
			$anoletivo = $aluno->ano_escolar;
			$button = 'Atualizar';
			$action = "../../routes/alunoAtualizado.php";
			$action_back = "../listagemAlunos/";
			$title = "Editar Aluno";
		}
	}
?>
<!DOCTYPE html>
<html>
<head>
	<title><?php echo $title ?></title>
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
	<link rel="stylesheet" type="text/css" href="styles.css">
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.1/jquery.min.js"></script>
	<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
	<link href="../../utils/bootstrap-datepicker/bootstrap-datepicker.css" rel="stylesheet"/>
	<script src="../../utils/bootstrap-datepicker/bootstrap-datepicker.min.js"></script> 
	<script src="../../utils/bootstrap-datepicker/bootstrap-datepicker.pt-BR.min.js" charset="UTF-8"></script>
</head>
<body>
	<div class="container">
		<div class="form-group">
			<h2><?php echo $title ?></h2>
		</div>
		<form class="" action='<?php echo $action ?>' method="POST" class="form-horizontal">
			<div class="form-group">
				<label>Aluno</label>
				<input type="number" class="form-control" name="id" value="<?php echo $id ?>" style="display: none">
				<input type="text" class="form-control" name="nome" placeholder="Nome Completo" required="" autocomplete="off" value="<?php echo $nome ?>">
			</div>
			<div class="form-group">
				<label>Data de Nascimento</label>
				<div class="input-group date">
					<input type="text" class="form-control" name="nascimento" id="date" placeholder="dd/mm/aaaa" required="" autocomplete="off" value="<?php echo $nascimento ?>">
					<div class="input-group-addon">
						<span class="glyphicon glyphicon-th"></span>
					</div>
				</div>
			</div>
			<div class="form-group">
				<label>Sexo</label>
				<select class="form-control form-control-sm" name="sexo" value=<?php $sexo ?>>
	  				<option value="m">M</option>
	  				<option value="f">F</option>
				</select>
			</div>
			<div class="form-group">
				<label>Ano Escolar</label>
				<select class="form-control form-control-sm" name="anoletivo" >
	  				<option value="1">1º Ano</option>
	  				<option value="2">2º Ano</option>
	  				<option value="3">3º Ano</option>
				</select>
			</div>
			<div class="buttons">
				<button class="btn btn-sm btn-primary" type="submit"><?php echo $button ?></button>
				<a href='<?php echo $action_back ?>' role="button" class="btn btn-sm btn-danger">Voltar</a>
			</div>
		</form>
	</div>
	<script type="text/javascript">
		$('#date').datepicker({	
			format: "dd/mm/yyyy",	
			language: "pt-BR",
			endDate: '-1d',
		});
	</script>
</body>
</html>