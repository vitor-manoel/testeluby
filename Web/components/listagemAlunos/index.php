<?php
	include '../../utils/verificarSessao.php';
    sessionCheck('../../index.php');
	include '../../../Backend/controllers/readAlunos.php';
	$lista = listaDeAlunos();
?>
<!DOCTYPE html>
<html>
<head>
	<title>Lista de Alunos</title>
	<link rel="stylesheet" type="text/css" href="styles.css">
	<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">
	<script src="https://kit.fontawesome.com/8875521770.js" crossorigin="anonymous"></script>
</head>
<body>
	<div class="container">
		<h2>Lista de Alunos</h2>
		<table class="table table-striped">
		  <thead>
		    <tr>
		      <th scope="col">Nome do Aluno</th>
		      <th scope="col">Idade</th>
		      <th scope="col">Sexo</th>
		      <th scope="col">Ano Letivo</th>
		      <?php
		      if($_SESSION['usuario']['acess'] > 0) {
		      ?>
		      <th scope="col" style="text-align: center;">Ações</th>
		  	  <?php } ?>
		    </tr>
		  </thead>
		    	<?php
		    		foreach (json_decode($lista) as $aluno ) {
		    			$idAluno = $aluno->id;
		    			$nome = $aluno->nome;
						$nascimento = $aluno->nasc;
						$idade = new DateTime($nascimento);
						$interval = $idade->diff(new DateTime(date('d-m-Y')));
		    			$sexo = $aluno->sexo;
		    			$ano = $aluno->ano_escolar;
		    	?>
		    <tr>
		      	<td><?php echo $nome ?></td>
		      	<td><?php echo $interval->format('%Y anos') ?></td>
		      	<td><?php echo strtoupper($sexo) ?></td>
		      	<td><?php echo $ano ?>º Ano</td>
		      	<?php
		      	if($_SESSION['usuario']['acess'] > 0) {
		      	?>
		      	<td>
				  <a type="button" class="btn btn-warning btn-sm" href="../formularioAluno/index.php?id=<?php echo $idAluno ?>"><i class="far fa-edit"></i>&nbsp;Editar</a> <a type="button" class="btn btn-danger btn-sm" href="../../routes/alunoDeletado.php?id=<?php echo $idAluno ?>"><i class="far fa-trash-alt"></i>&nbsp;Excluir</a>
				</td>
		      <?php }} ?>
		    </tr>
		</table>
		<div class="form-group">
			<a href="../../menu.php" role="button" class="btn btn-sm btn-primary">Voltar</a>
		</div>
	</div>
	<script src="https://code.jquery.com/jquery-3.4.1.slim.min.js" integrity="sha384-J6qa4849blE2+poT4WnyKhv5vZF5SrPo0iEjwBvKU7imGFAV0wwj1yYfoRSJoZ+n" crossorigin="anonymous"></script>
	<script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js" integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" crossorigin="anonymous"></script>
	<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js" integrity="sha384-wfSDF2E50Y2D1uUdj0O3uMBJnjuUD4Ih7YwaYd1iqfktj0Uod8GCExl3Og8ifwB6" crossorigin="anonymous"></script>
</body>
</html>