<?php
    function listaDeAlunos(){
        include '../../../Backend/conexao.php';

        $query = "SELECT * FROM alunos";

        $listar = mysqli_query($conexao,$query);

        $alunos_json = array();

        while ($row = mysqli_fetch_assoc($listar)) {
            $alunos_json[] = $row;
        }

        return json_encode($alunos_json);
    }

    function buscarAluno(){
        include '../../../Backend/conexao.php';
        
        $id = $_GET['id'];

        $query = "SELECT * FROM alunos WHERE id = $id";

        $busca = mysqli_query($conexao,$query);

        $alunos_json = array();

        while ($row = mysqli_fetch_assoc($busca)) {
            $alunos_json[] = $row;
        }

        return json_encode($alunos_json);
    }
?>